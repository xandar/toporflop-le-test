# TopOrFlop le test #

Bienvenue sur le teste technique de la société [Lexik](http://www.lexik.fr).  
Il s'agit d'un projet trés simple en Symfony2 auquel il faut apporter quelques modifications.

L'application permet à des utilisateurs identifiés de voter pour ou contre un médium.  
Une interface d'administration permet d 'ajouter des média.

- Il y a un bug dans le calcul des moyennes des notes des médias : il faudrait le corriger.
- Il faudrait rajouter une gestion des catégories avec les pré-requis suivants :
    - Chaque média doit appartenir à une seule catégorie.
    - Une catégorie est définie par un label uniquement.
    - La liste de catégorie est fixe et ne necessite pas d'interface d'administration.
    - Dans l'administration, il faut améliorer la gestion des média pour avoir un listing paginé des média.
    - Dans l'administration, l'ajout et l'édition d'un medium doit permettre le choix de sa catégorie.
    - En front, sur la page de détails d'un médium, il faut que la catégorie apparaisse.  
    Il faut qu'il y ai un lien sur le nom de la catégorie.  
    Ce lien donne accès à la liste de tous les médias de la catégorie trié par ordre chronologique.

Les évolutions doivent faire l'objet d'une "Pull Request" sur le dépôt principal (branche master).

Merci.
