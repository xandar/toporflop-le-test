<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
       $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $userDatas = array(
            'thomas' => array(
                'email'    => 'thomas@email.tld',
                'password' => 'password',
            ),
            'samuel' => array(
                'email'    => 'samuel@email.tld',
                'password' => 'password',
            ),
            'jeremy' => array(
                'email'    => 'jeremy@email.tld',
                'password' => 'password',
            ),
            'cedric' => array(
                'email'    => 'cedric@email.tld',
                'password' => 'password',
            ),
            'laurent' => array(
                'email'    => 'laurent@email.tld',
                'password' => 'password',
            ),
            'gilles' => array(
                'email'    => 'gilles@email.tld',
                'password' => 'password',
            ),
            'nicolas' => array(
                'email'    => 'nicolas@email.tld',
                'password' => 'password',
            ),
            'olivier' => array(
                'email'    => 'olivier@email.tld',
                'password' => 'password',
            ),
            'anthony' => array(
                'email'    => 'barreau.anthony@gmail.com',
                'password' => 'test',
            )
        );

        $userManager = $this->container->get('fos_user.user_manager');

        foreach ($userDatas as $userName => $userData) {
            $user = new User();
            $user->setUsername($userName);
            $user->setEmail($userData['email']);
            $user->setEnabled(true);
            $user->setPlainPassword($userData['password']);

            $userManager->updatePassword($user);

            $manager->persist($user);

            $this->addReference(
                sprintf('user-%s', $userName),
                $user
            );
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 30;
    }
}
