<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\Vote;

/**
 * Class LoadVoteData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadVoteData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $media = $manager->merge($this->getReference('media-'.$i));

            $vote1 = new Vote();
            $vote1->setScore(rand(1,10));
            $vote1->setUser($manager->merge($this->getReference('user-jeremy')));
            $media->addVote($vote1);

            $vote2 = new Vote();
            $vote2->setScore(rand(1,10));
            $vote2->setUser($manager->merge($this->getReference('user-laurent')));
            $media->addVote($vote2);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 40;
    }
}
