<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\Media;

/**
 * Class LoadMediaData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadMediaData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $lolcatCategory = $this->getReference("lolcat-category");
        $memeCategory   = $this->getReference("meme-category");
        $categories     = array($lolcatCategory, $memeCategory);

        $urls = array(
            'http://i0.kym-cdn.com/photos/images/facebook/000/002/110/longcat.jpg',
            'http://i1.kym-cdn.com/entries/icons/original/000/000/774/lime-cat.jpg',
            'http://shechive.files.wordpress.com/2011/05/hover-cat-20.jpg',
        );

        for ($i = 1; $i <= 10; $i++) {
            $image = new Media();
            $image->setTitle('image '.$i);
            $image->setUrl($urls[$i%3]);
            $image->setCategory($categories[rand(0, 1)]);

            $manager->persist($image);

            $this->addReference('media-'.$i, $image);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 20;
    }
}
