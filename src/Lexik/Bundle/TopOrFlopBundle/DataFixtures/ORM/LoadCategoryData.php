<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\Category;
use Lexik\Bundle\TopOrFlopBundle\Entity\Media;

/**
 * Fixtures for categories. Creates a bunch of new categories.
 *
 * Class LoadMediaData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $memeCategory = new Category();
        $memeCategory->setTitle("Meme");

        $lolcatsCategory = new Category();
        $lolcatsCategory->setTitle("Lolcats");

        $manager->persist($memeCategory);
        $manager->persist($lolcatsCategory);

        $this->setReference("lolcat-category", $lolcatsCategory);
        $this->setReference("meme-category", $memeCategory);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 10;
    }
}
