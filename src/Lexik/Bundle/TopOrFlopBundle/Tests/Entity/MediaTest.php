<?php

namespace Lexik\Bundle\TopOrFlopBundle\Tests\Entity;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;

/**
 * Media entity tests
 */
class MediaTest extends \PHPUnit_Framework_TestCase
{
    public function testDisplayedAverage()
    {
        $media = new Media();

        $media->setAverage(5);
        $this->assertEquals('5.0', $media->getDisplayedAverage());

        $media->setAverage(5.5);
        $this->assertEquals('5.5', $media->getDisplayedAverage());

        $media->setAverage(16/3);
        $this->assertEquals('5.3', $media->getDisplayedAverage());
    }
}
