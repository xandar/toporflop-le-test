<?php

namespace Lexik\Bundle\TopOrFlopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Media
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lexik\Bundle\TopOrFlopBundle\Repository\MediaRepository")
 */
class Media
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $url;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var float $average
     *
     * @ORM\Column(name="average", type="float", nullable=true)
     */
    private $average;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\Vote",
     *     mappedBy="media",
     *     cascade={"persist", "remove"}
     * )
     *
     * @Serializer\Exclude()
     */
    private $votes;

    /**
     * @var Category The parent category
     *
     * @ORM\ManyToOne(
     *      targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\Category",
     *      inversedBy="medias"
     * )
     *
     * @ORM\JoinColumn(
     *      name="category_id",
     *      referencedColumnName="id"
     * )
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->votes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param  string $url
     * @return Media
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Media
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set average
     *
     * @param float $average
     */
    public function setAverage($average)
    {
        $this->average = $average;
    }

    /**
     * Get average
     *
     * @return float
     */
    public function getAverage()
    {
        return $this->average;
    }

    /**
     * Add Vote
     *
     * @param Vote $vote
     */
    public function addVote(Vote $vote)
    {
        $vote->setMedia($this);
        $this->votes[] = $vote;

        $this->computeAverageScore();
    }

    /**
     * Get Votes
     *
     * @return ArrayCollection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Calculate the average score of the media
     */
    public function computeAverageScore()
    {
        $sum = array_reduce($this->getVotes()->toArray(), function($res, Vote $vote) {
            return $res + $vote->getScore();
        });

        $this->average = $sum / $this->getVotes()->count();
    }

    /**
     * Average score formatted for display
     *
     * @return string
     */
    public function getDisplayedAverage()
    {
        return (null === $this->average) ? '-' : sprintf('%.1f', $this->average);
    }

    /**
     * Whether the user has already voted for this media or not
     *
     * @param  User $user
     *
     * @return boolean
     */
    public function hasUserAlreadyVoted(User $user)
    {
        foreach ($this->votes as $vote) {
            if ($vote->getUser() == $user) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove votes
     *
     * @param \Lexik\Bundle\TopOrFlopBundle\Entity\Vote $votes
     */
    public function removeVote(\Lexik\Bundle\TopOrFlopBundle\Entity\Vote $votes)
    {
        $this->votes->removeElement($votes);
    }

    /**
     * Set category
     *
     * @param \Lexik\Bundle\TopOrFlopBundle\Entity\Category $category
     * @return Media
     */
    public function setCategory(\Lexik\Bundle\TopOrFlopBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Lexik\Bundle\TopOrFlopBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
