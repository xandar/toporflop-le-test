<?php

namespace Lexik\Bundle\TopOrFlopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as FOSUser;

/**
 * Class User
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class User extends FOSUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\Vote",
     *     mappedBy="user"
     * )
     */
    protected $votes;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->votes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add vote
     *
     * @param Vote $vote
     */
    public function addVote(Vote $vote)
    {
        $this->votes[] = $vote;
    }

    /**
     * Get Votes
     *
     * @return ArrayCollection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Remove votes
     *
     * @param \Lexik\Bundle\TopOrFlopBundle\Entity\Vote $votes
     */
    public function removeVote(\Lexik\Bundle\TopOrFlopBundle\Entity\Vote $votes)
    {
        $this->votes->removeElement($votes);
    }
}
