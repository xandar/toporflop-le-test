<?php

namespace Lexik\Bundle\TopOrFlopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form builder to create and edit a category
 *
 * Class CategoryType
 * @package Lexik\Bundle\TopOrFlopBundle\Form
 */
class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('save', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lexik\Bundle\TopOrFlopBundle\Entity\Category'
        ));
    }

    public function getName()
    {
        return 'lexik_bundle_toporflopbundle_categorytype';
    }
}
