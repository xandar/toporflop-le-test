<?php

namespace Lexik\Bundle\TopOrFlopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', 'url')
            ->add('title')
            ->add('category', 'entity', array(
                "class" => "LexikTopOrFlopBundle:Category",
                "property" => "title",
                "expanded" => false,
                "multiple" => false
            ))
            ->add('save', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lexik\Bundle\TopOrFlopBundle\Entity\Media'
        ));
    }

    public function getName()
    {
        return 'lexik_bundle_toporflopbundle_mediatype';
    }
}
