<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Form\VoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class DefaultController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('LexikTopOrFlopBundle:Default:index.html.twig', array(
            'name' => 'Lexik',
            'days' => array('monday', 'tuesday', 'wednesday', 'thursday', 'friday'),
            'html' => '<b>ce texte n\'est pas en gras!</b>',
        ));
    }

    /**
     * @Route("/show", name="show_random")
     *
     * @return RedirectResponse
     *
     * @throws NotFoundHttpException
     */
    public function showRandomMediaAction()
    {
        $media = $this->get('lexik_top_or_flop.media_manager')->getNextMedia();

        if (!$media instanceof Media) {
            throw $this->createNotFoundException('No Media found.');
        }

        return $this->redirect($this->generateUrl('show_media', array('id' => $media->getId())));
    }

    /**
     * @Route("/show/{id}", name="show_media")
     *
     * @param integer $id
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function showMediaAction($id)
    {
        $mediaManager = $this->get('lexik_top_or_flop.media_manager');

        $media = $mediaManager->getMedia($id);
        if ($media === null) {
            throw $this->createNotFoundException();
        }

        if (null !== $vote = $mediaManager->getNewVote($media)) {
            $form = $this->createForm(new VoteType(), $vote);
        }

        return $this->render('LexikTopOrFlopBundle:Default:show.html.twig', array(
            'media' => $media,
            'form'  => isset($form) ? $form->createView() : null,
        ));
    }

    /**
     * @Route("/vote/{id}", name="vote_media")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param integer $id
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function voteMediaAction(Request $request, $id)
    {
        $mediaManager = $this->get('lexik_top_or_flop.media_manager');

        $media = $mediaManager->getMedia($id);
        if ($media === null) {
            throw $this->createNotFoundException();
        }

        $vote = $mediaManager->getNewVote($media);

        $form = $this->createForm(new VoteType(), $vote);

        $form->handleRequest($request);

        if ($form->isValid()) {
            // $vote is saved by Media cascade persist
            $media->addVote($vote);
            $this->get('doctrine.orm.entity_manager')->flush();

            $this->get('session')->getFlashBag()->add('success', 'Votre vote est enregistré');

            return $this->redirect($this->generateUrl('show_media', array(
                'id' => $media->getId(),
            )));
        }

        $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenue');

        return $this->forward('LexikTopOrFlopBundle:Default:showMedia', array(
            'id' => $media->getId(),
        ));
    }

    /**
     * @Route("/tops", name="show_tops")
     *
     * @return Response
     */
    public function showTopsAction()
    {
        $tops = $this->get('doctrine.orm.entity_manager')
            ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
            ->getTopMedias();

        return $this->render('LexikTopOrFlopBundle:Default:tops.html.twig', array(
            'tops'  => $tops,
        ));
    }

    /**
     * @Route("/flops", name="show_flops")
     *
     * @return Response
     */
    public function showFlopsAction()
    {
        $flops = $this->get('doctrine.orm.entity_manager')
            ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
            ->getFlopMedias();

        return $this->render('LexikTopOrFlopBundle:Default:flops.html.twig', array(
            'flops' => $flops,
        ));
    }

    /**
     * Show all medias belonging to the specified category
     *
     * @Route("/category/{id}", name="show_media_from_category")
     *
     * @return Response
     */
    public function showMediasFromCategoryAction($id)
    {
        // first we get the category
        $category = $this->get('doctrine.orm.entity_manager')
            ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Category')
            ->find($id);

        // if the category doesn't exists, error
        if($category == null)
        {
            $this->get("session")->getFlashBag()->add("error", "Catégorie introuvable.");
            return $this->redirect($this->generateUrl("index"));
        }

        // then we fetch the medias with the specified category
        $medias = $this->get('doctrine.orm.entity_manager')
            ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
            ->findByCategory($category, array("id" => "ASC"));

        return $this->render('LexikTopOrFlopBundle:Default:medias_from_category.html.twig', array(
            'medias'  => $medias,
            'category' => $category
        ));
    }
}
