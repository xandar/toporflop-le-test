<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Lexik\Bundle\TopOrFlopBundle\Entity\Category;
use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Form\CategoryType;
use Lexik\Bundle\TopOrFlopBundle\Form\MediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Intl\Exception\NotImplementedException;

/**
 * Class BackendController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 *
 * @Route("/backend")
 */
class BackendController extends Controller
{
    /**
     * Shows the new media form page
     *
     * @Route("/media/new", name="backend_media_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newMediaAction(Request $request)
    {
        $media = new Media();

        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_media_new'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new_media.html.twig', array(
            'form'  => $form->createView(),
        ));
    }

    /**
     * Shows the media list page
     *
     * @Route("/", name="backend_media_list")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function mediasAction(Request $request)
    {
        $manager    = $this->getDoctrine()->getManager();
        $medias     = $manager->getRepository("LexikTopOrFlopBundle:Media")->findAll();

        return $this->render("LexikTopOrFlopBundle:Backend:medias.html.twig", array(
            "medias" => $medias
        ));
    }

    /**
     * Shows the media edit page and/or updates a media
     *
     * @Route("/media/edit/{id}", requirements={"id" = "\d+"}, name="backend_media_edit")
     *
     * @param $id
     * @internal param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editMediaAction($id)
    {
        // first we get the media
        $manager    = $this->getDoctrine()->getManager();
        $media      = $manager->getRepository("LexikTopOrFlopBundle:Media")->find($id);

        if($media == null)
        {
            $this->get("session")->getFlashBag()->add("error", "Le média demandé est introuvable.");
            return $this->redirect($this->generateUrl("backend_media_list"));
        }

        // then we create a form using the media values
        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($this->getRequest());

        // if form was submitted, we update the media in the database
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Média correctement mis à jour.");

            return $this->redirect($this->generateUrl('backend_media_list'));
        }

        // form wasn't submitted yet, so let's display it
        return $this->render('LexikTopOrFlopBundle:Backend:new_media.html.twig', array(
            'form'  => $form->createView(),
        ));
    }

    /**
     * Deletes a media from the database
     *
     * @Route("/media/delete/{id}", requirements={"id" = "\d+"}, name="backend_media_delete")
     *
     * @param $id
     * @internal param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return RedirectResponse|Response
     */
    public function deleteMediaAction($id)
    {
        // first we get the media
        $manager    = $this->getDoctrine()->getManager();
        $media      = $manager->getRepository("LexikTopOrFlopBundle:Media")->find($id);

        if($media == null)
        {
            $this->get("session")->getFlashBag()->add("error", "Le média demandé est introuvable.");
            return $this->redirect($this->generateUrl("backend_media_list"));
        }

        // then we remove it
        $manager->remove($media);
        $manager->flush();

        $this->get("session")->getFlashBag()->add("success", "Media correctement supprimé.");

        return $this->redirect($this->generateUrl("backend_media_list"));
    }

    /**
     * Shows the new category form page
     *
     * @Route("/category/new", name="backend_category_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newCategoryAction(Request $request)
    {
        $category = new Category();

        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($category);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Catégorie correctement ajoutée.");

            return $this->redirect($this->generateUrl('backend_category_new'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new_category.html.twig', array(
            'form'  => $form->createView(),
        ));
    }

    /**
     * Shows the category list page
     *
     * @Route("/categories", name="backend_category_list")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function categoriesAction(Request $request)
    {
        $manager    = $this->getDoctrine()->getManager();
        $categories = $manager->getRepository("LexikTopOrFlopBundle:Category")->findAll();

        return $this->render("LexikTopOrFlopBundle:Backend:categories.html.twig", array(
            "categories" => $categories
        ));
    }

    /**
     * Shows the category edit page and/or updates a category
     *
     * @Route("/category/edit/{id}", requirements={"id" = "\d+"}, name="backend_category_edit")
     *
     * @param $id
     * @internal param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editCategoryAction($id)
    {
        // first we get the category
        $manager    = $this->getDoctrine()->getManager();
        $category   = $manager->getRepository("LexikTopOrFlopBundle:Category")->find($id);

        if($category == null)
        {
            $this->get("session")->getFlashBag()->add("error", "La catégorie demandée est introuvable.");
            return $this->redirect($this->generateUrl("backend_category_list"));
        }

        // then we create a form using the category values
        $form = $this->createForm(new CategoryType(), $category);
        $form->handleRequest($this->getRequest());

        // if form was submitted, we update the category in the database
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($category);
            $em->flush();

            $this->get("session")->getFlashBag()->add("success", "Catégorie correctement mise à jour.");

            return $this->redirect($this->generateUrl('backend_category_list'));
        }

        // form wasn't submitted yet, so let's display it
        return $this->render('LexikTopOrFlopBundle:Backend:new_category.html.twig', array(
            'form'  => $form->createView(),
        ));
    }

    /**
     * Deletes a category from the database
     *
     * @Route("/category/delete/{id}", requirements={"id" = "\d+"}, name="backend_category_delete")
     *
     * @param $id
     * @internal param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return RedirectResponse|Response
     */
    public function deleteCategoryAction($id)
    {
        // first we get the category
        $manager    = $this->getDoctrine()->getManager();
        $category   = $manager->getRepository("LexikTopOrFlopBundle:Category")->find($id);

        if($category == null)
        {
            $this->get("session")->getFlashBag()->add("error", "La catégorie demandée est introuvable.");
            return $this->redirect($this->generateUrl("backend_media_list"));
        }

        // then we remove it
        $manager->remove($category);
        $manager->flush();

        $this->get("session")->getFlashBag()->add("success", "Catégorie correctement supprimée.");

        return $this->redirect($this->generateUrl("backend_category_list"));
    }
}
